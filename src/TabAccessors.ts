type TabID = number;
export abstract class TabAccessor {
	public abstract getLastAccessed(tabId: TabID): Promise<Date | undefined>;

	private static _singleton: TabAccessor;
	public static get Singleton(): TabAccessor {
		if (!TabAccessor._singleton) {
			if (chrome) {
				TabAccessor._singleton = new ChromeTabAccessor();
			}
			else {
				TabAccessor._singleton = new FirefoxTabAccessor();
			}
		}

		return TabAccessor._singleton;
	}
}

interface ChromeOnActivateHandler { (activeInfo: chrome.tabs.TabActiveInfo): void }

export class ChromeTabAccessor extends TabAccessor {
	private chromeLastAccessed: Map<TabID, Date>;
	private lastTab: TabID | undefined;
	private _tabTracker: ChromeOnActivateHandler | undefined;

	constructor() {
		super();
		this.chromeLastAccessed = new Map<TabID, Date>();

		// check if we're in unit test?
		if (global.chrome) {
			chrome.tabs.onActivated.addListener(this.tabTracker);
		}
	}

	public get tabTracker(): ChromeOnActivateHandler {
		if (this._tabTracker) {
			return this._tabTracker;
		}

		return (this._tabTracker = (activeInfo: chrome.tabs.TabActiveInfo) => {
			if (this.lastTab) {
				this.chromeLastAccessed.set(this.lastTab, new Date());
			}
			this.lastTab = activeInfo.tabId;
		})
	}

	public async getLastAccessed(tabId: TabID): Promise<Date | undefined> {
		return this.chromeLastAccessed.get(tabId);
	}
}

export class FirefoxTabAccessor extends TabAccessor {
	public async getLastAccessed(tabId: TabID): Promise<Date | undefined> {
		const tabInfo = await browser.tabs.get(tabId);
		const lastAccessedMs = tabInfo.lastAccessed;
		return lastAccessedMs ? new Date(lastAccessedMs) : (void 0);
	}
}
