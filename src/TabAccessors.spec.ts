import 'mocha';
import { expect } from 'chai';
import { ChromeTabAccessor } from './TabAccessors';

describe("ChromeTabAccessor", () => {
    it("constructs without errors", () => {
        const accessor: ChromeTabAccessor = new ChromeTabAccessor();
    });

    describe("tabTracker", () => {
        let accessor: ChromeTabAccessor;

        // Manually made Mock Date class
        // TODO replace with actual Mock/Spy framework?
        let _date: DateConstructor = Date;
        let mockDates: Date[] = [];
        let MockDate = () => {
            return mockDates.pop();
        }
        beforeEach(() => {
            accessor = new ChromeTabAccessor();
        });

        it("keeps track of the first tabId", async () => {
            // Setup
            (global.Date as any) = MockDate;
            let mock1 = new _date(1);
            const oldTab = 1;
            const newTab = 2;
            mockDates.push(mock1);

            // Run
            accessor.tabTracker({
                tabId: oldTab,
                windowId: 1
            });
            accessor.tabTracker({
                tabId: newTab,
                windowId: 1
            });

            // Check
            const oldTabRet = await accessor.getLastAccessed(oldTab);
            expect(oldTabRet).is.not.undefined;
            expect(oldTabRet?.getTime()).equal(1);

            const newTabRet = await accessor.getLastAccessed(newTab);
            expect(newTabRet).is.undefined;
        })
    
        it("returns same function in subsequent calls", () => {
            const trackerOne = accessor.tabTracker;
            expect(trackerOne).to.be.a('function');
            const trackerTwo = accessor.tabTracker
            expect(trackerTwo).to.equal(trackerOne);
        })
    });
});

describe("FirefoxTabAccessor", () => {
});