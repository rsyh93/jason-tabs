import { TabAccessor } from "./TabAccessors";

console.log("Background script initialized")

const CHECK_TIMEOUT = 5 * 60 * 1000; // 5 min in ms

export const enum PortNames {
	getLastAccessed = "getLastAccessed",
}

/** 
 * Message format used for getLastAccesssed Port
 * @param tabIds Array of tab ids to get last accessed date
 */
export interface GetLastAccessedRequest {
	tabIds?: number[], // 
}

/** 
 * Response format used for getLastAccesssed Port
 */
export interface GetLastAccessedResponse {[tabId: number]: Date|undefined};

const accessor = TabAccessor.Singleton;

let _onMessage: 
	chrome.runtime.ExtensionMessageEvent | 
	WebExtEvent<(message: any, sender: browser.runtime.MessageSender, sendResponse: (response?: any) => void) => boolean | void | Promise<any>>;

if (chrome) {
	_onMessage = chrome.runtime.onMessage;
}
else {
	_onMessage = browser.runtime.onMessage;
}

_onMessage.addListener(
  function(request: any, _sender: any, sendResponse: (arg0: any) => void) {
	if (!(request && request.tabIds && Array.isArray(request.tabIds))) {
		return false;
	}

	(async () => {
		const map: GetLastAccessedResponse = {};
		for (let i = 0; i < request.tabIds.length; i++) {
			const tabId = request.tabIds[i];
			map[tabId] = await accessor.getLastAccessed(tabId);
		}
		sendResponse(map);
	})();
	return true;
  }
);
