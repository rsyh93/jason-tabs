import * as React from "react";
import { GetLastAccessedResponse, PortNames } from "./background";

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <TabInfoTable />
    </div>
  );
};

type BrowserTab = chrome.tabs.Tab | browser.tabs.Tab;
interface TabInfoTableState { 
  tabs: BrowserTab[];
  lastAccessed: {[tabId: number]: Date|undefined};
}
class TabInfoTable extends React.Component<{}, TabInfoTableState> {
  private _portGetLastAccessed?: chrome.runtime.Port;
  constructor(props: {} | Readonly<{}>) {
    super(props);
    this.state = {
      tabs: [],
      lastAccessed: {}
    }
  }

  componentDidMount() {
    this.getTabInfo()
  }

  // TODO abstract away browser-specific clauses
  private async getTabInfo() {
    let tabs: chrome.tabs.Tab[] | browser.tabs.Tab[];
    if (chrome) {
      tabs = await new Promise((resolve) => chrome.tabs.query({}, resolve));
    }
    else {
      tabs = await browser.tabs.query({});
    }
    if (chrome) {
      const request = { 
        tabIds: (tabs as Array<BrowserTab>)
          .map((tab) => tab.id)
          .filter((tabId) => !!tabId)
      };
      const responseFunction = (response: GetLastAccessedResponse) => {
        this.setState({
          tabs: ((tabs as Array<BrowserTab>).filter((tab) => !!tab.id)),
          lastAccessed: response,
      })};

      chrome.runtime.sendMessage(request, responseFunction);
    }
  }

  render() {
    return (
      <table>
        <tr><th style={{width: "200px"}}>ID</th><th style={{width: "200px"}}>Name</th></tr>
        { 
          this.state.tabs.map((tab) => 
            <TabInfoRow 
              tabId={tab.id} 
              title={tab.title}
              lastAccessed={((tab as browser.tabs.Tab).lastAccessed) ? 
                new Date((tab as browser.tabs.Tab).lastAccessed || 0) : 
                (tab.id ? this.state.lastAccessed[tab.id] : void 0)}
            />
          )
        }
      </table>
    );
  }
}

interface TabInfoRowProps {
  tabId?: number;
  title?: string;
  lastAccessed?: Date;
}
class TabInfoRow extends React.Component<TabInfoRowProps> {
  componentDidMount() {
  }

  render() {
    return (<tr>
      <td>{ this.props.title }</td>
      <td>{ this.props.lastAccessed?.toLocaleString() }</td>
    </tr>);
  }
}

export default App;