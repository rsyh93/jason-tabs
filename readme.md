# Jason's Tabs

Jason's tab problem is getting out of hand. This extension will fix that, one way or another.

## Build

1. Install `npm`
2. Clone the repository
3. Run `npm install` to install dependencies
4. Run `npm run-script build` to compile

## Install

### Chrome

1. Navigate to `chrome://extensions`
2. Toggle "Developer mode"
3. Click "Load unpacked"
4. Select the `dist` folder

### Firefox

1. Navigate to `about:debugging`
2. Click "This Firefox"
3. Under "Temporary Extensions", click "Load Temporary Add-on..."
4. Select any file under the `dist` folder

## Todo

- [ ] Actually test in Chrome
- [ ] Replace placeholder icons
- [ ] Set up script to package prod version
- [ ] Unit testing
- [ ] Set up hot loading?
